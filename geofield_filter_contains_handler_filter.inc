<?php

/**
 * @file
 * Filter handler class for Geofield Filter Contains.
 */

class geofield_filter_contains_handler_filter extends views_handler_filter {

  /**
   * Override parent::query().
   */
  function query() {

    // First check to see if we need to use a different filter.
    if ($this->options['geofield_filter_contains_other']) {
      $filter_name = $this->options['geofield_filter_contains_other_filter'];
      $filters = $this->view->display_handler->get_handlers('filter');
      if (!empty($filters[$filter_name])) {
        $query_parameter = $filters[$filter_name]->options['expose']['identifier'];
        if (!empty($this->view->exposed_input[$query_parameter])) {
          $this->value = $this->view->exposed_input[$query_parameter];
          // Support for the Geofield proximity filters.
          if (is_array($this->value) && !empty($this->value['origin'])) {
            $this->value = $this->value['origin'];
          }
        }
      }
    }

    if (empty($this->value)) {
      return;
    }

    // In exposed forms, the value is in an array, for some reason.
    if (is_array($this->value)) {
      $this->value = reset($this->value);
    }

    // Sanitize the user input.
    $this->value = check_plain($this->value);

    // Get the latitude/longitude from Google.
    $point = geocoder('google', $this->value);
    if (empty($point->coords)) {
      return;
    }

    // Construct the MySQL expression.
    $table = $this->ensure_my_table();
    $column = $this->definition['field_name'] . '_geom';
    $point = "POINT({$point->coords[0]} {$point->coords[1]})";
    $snippet = "ST_Contains(ST_GeomFromWKB($table.$column), ST_GeomFromText(:point))";

    // Add the expression to the Views query.
    $group = $this->options['group'];
    $args = array(':point' => $point);
    $this->query->add_where_expression($group, $snippet, $args);
  }

  /**
   * Override parent::value_form().
   */
  function value_form(&$form, &$form_state) {

    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Location'),
      '#default_value' => $this->value,
    );
  }

  /**
   * Override parent::options_form().
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    // A checkbox to indicate whether this filter should take its value from
    // a different filter. This is useful, for example, to piggy-back off of an
    // exposed proximity filter.
    $default_value = FALSE;
    if (isset($this->options['geofield_filter_contains_other'])) {
      $default_value = $this->options['geofield_filter_contains_other'];
    }
    $form['geofield_filter_contains_other'] = array(
      '#type' => 'checkbox',
      '#title' => t('Take value from an exposed filter'),
      '#default_value' => $default_value,
    );

    // A select list of the other exposed filters in this View.
    $options = array('' => t('None'));
    $filters = $this->view->display_handler->get_handlers('filter');
    foreach ($filters as $filter_name => $filter) {
      if ($filter->is_exposed()) {
        $label = $filter->options['expose']['label'];
        if (empty($label)) {
          $label = $filter_name;
        }
        $options[$filter_name] = $label;
      }
    }
    $default_value = '';
    if (isset($this->options['geofield_filter_contains_other_filter'])) {
      $default_value = $this->options['geofield_filter_contains_other_filter'];
    }
    $form['geofield_filter_contains_other_filter'] = array(
      '#type' => 'select',
      '#title' => 'Exposed filter to take value from',
      '#options' => $options,
      '#default_value' => $default_value,
      '#states' => array(
        'visible' => array(
          'input[name="options\\[geofield_filter_contains_other\\]"]' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    );
  }

  /**
   * Override parent::options_submit().
   */
  function options_submit(&$form, &$form_state) {

    parent::options_submit($form, $form_state);

    // Save our custom options.
    $options = array(
      'geofield_filter_contains_other',
      'geofield_filter_contains_other_filter',
    );
    foreach ($options as $option) {
      $this->options[$option] = $form_state['values']['options'][$option];
    }
  }
}
