<?php

/**
 * @file
 * Views hooks for Geofield Filter Contains.
 */

/**
 * Implements hook_field_views_data().
 */
function geofield_filter_contains_field_views_data_alter(&$result, $field, $module) {

  if ('geofield' != $module) {
    return;
  }

  $field_name = $field['field_name'];

  foreach ($result as $table_name => $table_data) {
    if (isset($table_data[$field_name])) {
      $group_name = $table_data[$field_name]['group'];
      $title = $table_data[$field_name]['title'] . " ($field_name) - contains";
      $result[$table_name]['field_geofield_filter_contains'] = array(
        'group' => $group_name,
        'title' => $title,
        'title short' => $title,
        'help' => $table_data[$field_name]['help'],
        'filter' => array(
          'field' => 'field_geofield_filter_contains',
          'table' => $table_name,
          'handler' => 'geofield_filter_contains_handler_filter',
          'field_name' => $field['field_name'],
          'real_field' => $table_name,
        ),
      );
    }
  }
}
