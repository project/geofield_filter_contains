# Geofield Filter Contains

## Overview

This module provides a Views filter that can be used to query entities that use Geofield for geographic regions. The filter takes a geographic point (such as a mailing address) and returns all the entities whose Geofield data contains that point.

## Requirements

* MySQL database (though I haven't tried anything else)
* [Views](https://www.drupal.org/project/views) module
* [Geofield](https://www.drupal.org/project/geofield) module
* [Geocoder](https://www.drupal.org/project/geocoder) module

## Use cases

### What county am I in?

A site has a node type called "County" that contains a Geofield populated with geographic boundaries. Users need to be able to enter their address and see the County that they live in.

### Nearby courts in my district

A site has a node type called "Court" that countains a Geofield populated with a geographic point (a single address). The site also has a taxonomy vocabulary called "District" that countains a Geofield populated with geographic boundaries. Users need to be able to enter their address and see the Courts nearby, sorted by proximity, but *only* the ones that are within the user's District.

## Installation

Enable the module in the usual way.

## Basic usage

1. Make sure you have an entity type/bundle (such as a node type or a taxonomy vocabulary) that contains a Geofield configured to accept region data, such as WKT or GeoJSON.
2. Create a View that can query the entity type/bundle from #1.
3. Add a filter and search for "contains" to narrow down the list, selecting the "contains" filter for the Geofield in question.
4. Save the view.

## Ways to get the geographic point

The module uses a single geographic point to find Geofield regions that contain it. There are 3 ways to provide the View with that point:

1. You can hardcode a value directly in the View filter
2. You can "Expose" the filter so that users will be able to enter a value
3. You can configure the filter to take its value from a different exposed filter (such as a Geofield proximity filter)

Note that all 3 of these methods send the value to a Google service for geocoding into latitude/longitude.

## Roadmap

* Tests
* Provide a more pluggable geocoding setup, for alternatives to the Google service
* Confirm whether MySQL is really required, or if PostgreSQL might also work
* Similar type of thing for Solr, for integration with `search_api_location` module.
* D8 version
